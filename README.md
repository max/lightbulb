<div align="center">
    <img src="https://gitlab.gnome.org/mdwalters/lightbulb/-/raw/main/data/icons/hicolor/scalable/apps/page.codeberg.mdwalters.LightBulb.svg" alt="The Light Bulb logo">
    <h1>Light Bulb</h1>
    Learn how to get around GNOME
</div>

## Installing

0. Install GNOME Builder (skip to #1 if already installed)
1. Open GNOME Builder
2. Click on `Clone repository...`
3. Set `Repository URL` to `https://gitlab.gnome.org/mdwalters/lightbulb.git`
4. Click on the dropdown menu beside the `Run` button
5. Click on the build icon on the header bar
6. Wait a few moments...
7. Click on the `Export bundle` button inside the dropdown
8. On the Files window that opens, double click on `page.codeberg.mdwalters.LightBulb.flatpak`
9. On the Software window, click on `Install`
10. You have installed Light Bulb
