# main.py
#
# Copyright 2023 Max Walters
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import gi
import subprocess

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")

from gi.repository import Gtk, Gio, Adw
from .window import LightBulbWindow

import locale
import gettext

class LightBulbApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(application_id="page.codeberg.mdwalters.LightBulb",
                         flags=Gio.ApplicationFlags.DEFAULT_FLAGS)
        self.create_action("quit", lambda *_: self.quit(), ["<primary>q"])
        self.create_action("about", self.on_about_action)
        self.create_action("tour", lambda *_: subprocess.check_output("gnome-tour", shell=True))

        locale.bindtextdomain("lightbulb", "/app/share/locale")
        locale.textdomain("lightbulb")
        gettext.install("lightbulb", "/app/share/locale")

    def do_activate(self):
        """Called when the application is activated.

        We raise the application"s main window, creating it if
        necessary.
        """
        win = self.props.active_window
        if not win:
            win = LightBulbWindow(application=self)
        win.present()

    def on_about_action(self, widget, _):
        """Callback for the app.about action."""
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name=gettext.gettext("Light Bulb"),
                                application_icon="page.codeberg.mdwalters.LightBulb",
                                developer_name="Max Walters",
                                version="45.0",
                                developers=[
                                    "Max Walters https://mdwalters.codeberg.page/"
                                ],
                                translator_credits="Max Walters https://mdwalters.codeberg.page/",
                                license_type=Gtk.License.GPL_3_0,
                                issue_url="https://gitlab.gnome.org/mdwalters/lightbulb/-/issues",
                                release_notes="""
                                <ul>
                                    <li>Initial preview release!</li>
                                </ul>
                                """,
                                debug_info=f"""Light Bulb version: 45.0
libadwaita version: {Adw.get_major_version()}.{Adw.get_minor_version()}.{Adw.get_micro_version()}
GTK version: {Gtk.get_major_version()}.{Gtk.get_minor_version()}.{Gtk.get_micro_version()}"""
        )
        about.add_acknowledgement_section("Concept design by", ["Tobias Bernard https://tobiasbernard.com/"])
        about.present()

    def create_action(self, name, callback, shortcuts=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """The application"s entry point."""
    app = LightBulbApplication()
    return app.run(sys.argv)
